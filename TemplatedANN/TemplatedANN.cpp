// TemplatedANN.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FullyConnectedNetwork.h"
#include "NetworkTrainer.h"

#include <iomanip>
#include <chrono>

std::array<float, 4> regression_function(std::array<float, 4> & Input) {
	Input[0] = (rand() % 100) / 100.0;
	Input[1] = (rand() % 100) / 100.0;
	Input[2] = (rand() % 100) / 100.0;
	Input[3] = (rand() % 100) / 100.0;

	std::array<float, 4> Out;
	Out[0] = Input[0];
	Out[1] = Input[1] + Out[0];
	Out[2] = Input[2] + Out[1];
	Out[3] = Input[3] + Out[2];

	return Out;
}
 
float round_place(float value, int places) {
	float offset = std::pow(10, places);
	int intermediate = std::floor(value * offset);
	float val = intermediate / offset;
	return val;
}

constexpr float TrainingTreshold = 0.002;
constexpr size_t InputSize = 4;
constexpr size_t OutputSize = 4;
using NetworkType = NeuralNetwork::FeedForwardNetwork<float, NeuralNetwork::ActivationFunction::LINEAR, InputSize, 4, OutputSize>;


int main()
{
	NetworkType * FFN = new NetworkType();

	std::array<float, InputSize> Input = { 0 };
	std::array<float, OutputSize> Output = { 0 };
	std::array<float, OutputSize> ActualOutput = { 0 };

	constexpr NeuralNetwork::ErrorFunction ErrorType = NeuralNetwork::ErrorFunction::SquaredError;

	NeuralNetwork::NetworkTrainer * Trainer = new NeuralNetwork::NetworkTrainer();
	Trainer->randomize_weights<ErrorType>(*FFN, 0.1f);

	size_t Iterations = 0, Consistency = 0;
	auto t1 = std::chrono::high_resolution_clock::now();
	for (;;) {
		float OverallError = 0;
		for (int i = 0; i < 100; ++i) {
			Output = regression_function(Input);

			//Intellisense is complaining that this is wrong, but it compiles
			Trainer->train_backpropagation<ErrorType>(*FFN, Input, Output, 0.01f);

			FFN->compute(Input, ActualOutput);
			OverallError += std::abs(NeuralNetwork::computeErrorSummation<ErrorType>(ActualOutput, Output));
			++Iterations;
		}
		OverallError /= 100;
		printf("Network Average Error: %f\n", OverallError);
		if (OverallError <= TrainingTreshold) {
			++Consistency;
			if (Consistency > 5) {
				break;
			}
		}
		else {
			Consistency = 0;
		}
	}
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> fp_ms = t2 - t1;
	printf("Training Complete, required %i iterations and %f ms\n", Iterations, fp_ms.count());

	for (int i = 0; i < 10; ++i) {
		Output = regression_function(Input);



		FFN->compute(Input, ActualOutput);
		std::cout << "Input: {";
		for (size_t i = 0; i < InputSize; ++i) {
			printf("%4.2f,", Input[i]);
		}
		std::cout << "}\tIdeal: {";
		for (size_t i = 0; i < OutputSize; ++i) {
			printf("%4.2f,", Output[i]);
		}
		std::cout << "}\tNetwork Output: {";
		for (size_t i = 0; i < OutputSize; ++i) {
			printf("%4.2f,", ActualOutput[i]);
		}
		printf("} = Error %f\n", std::abs(NeuralNetwork::computeErrorSummation<ErrorType>(ActualOutput, Output)));
		++Iterations;
	}

	delete(Trainer);
	delete(FFN);
	std::cin.get();
	//*/
	return 0;
}