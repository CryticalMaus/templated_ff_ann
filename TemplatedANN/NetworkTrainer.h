#pragma once

#include <vector>
#include <array>
#include <random>

#include "FullyConnectedNetwork.h"
#include "NetworkUtilities.h"

namespace NeuralNetwork {

	class NetworkTrainer {

		NetworkTrainer(const NetworkTrainer &) = delete;

		//GENETIC
		template<class ANNType, ErrorFunction ErrorType, typename DataType, ActivationFunction ActFunc, size_t ... LayerSizes>
		class internalGeneticTrainer {
			using StaticThis = internalGeneticTrainer<ANNType, ErrorType, DataType, ActFunc, LayerSizes...>;

			static constexpr auto HiddenLayerSizes = std::make_tuple(LayerSizes...);
			static constexpr size_t FirstHiddenLayerSize = std::get<1>(HiddenLayerSizes);

			std::default_random_engine generator;

			template<size_t Index>
			void recursiveRandomizeWeights(ANNType & FFN, std::uniform_real_distribution<DataType> distribution) {
				constexpr size_t rows = std::get<Index + 1>(StaticThis::HiddenLayerSizes);
				constexpr size_t cols = std::get<Index>(StaticThis::HiddenLayerSizes);//

				std::array<std::array<DataType, cols + 1>, rows> & WeightMatrix = std::get<Index>(FFN.WeightMatricies);

				for (size_t y = 0; y < rows; ++y) {
					for (size_t x = 0; x < cols + 1; ++x) {
						WeightMatrix[y][x] = distribution(this->generator);
					}
				}

				this->recursiveRandomizeWeights<Index - 1>(FFN, distribution);
			}

			template<>
			void recursiveRandomizeWeights<0>(ANNType & FFN, std::uniform_real_distribution<DataType> distribution) {
				constexpr size_t rows = std::get<1>(StaticThis::HiddenLayerSizes);
				constexpr size_t cols = std::get<0>(StaticThis::HiddenLayerSizes);//

				std::array<std::array<DataType, cols + 1>, rows> & WeightMatrix = std::get<0>(FFN.WeightMatricies);

				for (size_t y = 0; y < rows; ++y) {
					for (size_t x = 0; x < cols + 1; ++x) {
						WeightMatrix[y][x] = distribution(this->generator);
					}
				}
			}

		public:
			internalGeneticTrainer() {}
			~internalGeneticTrainer() {}

			void randomizeWeights(ANNType & FFN, DataType Bounds) {
				std::uniform_real_distribution<DataType> distribution(-Bounds, Bounds);
				this->recursiveRandomizeWeights<ANNType::WeightMatrixCount - 1>(FFN, distribution);
			}

		};

		//BACK PROPAGATION
		template<ActivationFunction ActFunc, size_t rows, size_t cols, typename DataType>
		static void backpropagate(const std::array<DataType, cols> & Input, const std::array<DataType, rows> & NetworkOutputLayer,
			const std::array<DataType, rows> & ErrorSignal, std::array<DataType, cols> & ResultErrorSignal, std::array<std::array<DataType, cols + 1>, rows> & WeightMatrix,
			DataType LearningRate) {

			std::array<DataType, rows> OutputNodeErrorContribution;
			applyErrorToActivationDerivative<ActFunc>(NetworkOutputLayer, ErrorSignal, OutputNodeErrorContribution);

			ResultErrorSignal = { 0 };

			for (size_t y = 0; y < rows; ++y) {
				for (size_t x = 0; x < cols; ++x) {
					DataType dNetdW = Input[x];//net change with weight
					DataType dErrdW = OutputNodeErrorContribution[y] * dNetdW;//error change with weight

					ResultErrorSignal[x] += OutputNodeErrorContribution[y] * WeightMatrix[y][x];//total error as result of input node

					WeightMatrix[y][x] -= dErrdW * LearningRate;
				}
				//bias term
				//not sure why I had this, why would the error from the bias term propagate backwards?
				//ResultErrorSignal[cols] += OutputNodeErrorContribution[y] * WeightMatrix[y][cols];

				WeightMatrix[y][cols] -= OutputNodeErrorContribution[y] * LearningRate;
			}
		}

		//INTERNAL BACK PROP TRAINER
		template<class ANNType, ErrorFunction ErrorType, typename DataType, ActivationFunction ActFunc, size_t ... LayerSizes>
		class internalBackPropagationTrainer {
			using StaticThis = internalBackPropagationTrainer<ANNType, ErrorType, DataType, ActFunc, LayerSizes...>;

			static constexpr auto HiddenLayerSizes = std::make_tuple(LayerSizes...);
			static constexpr size_t FirstHiddenLayerSize = std::get<1>(HiddenLayerSizes);
			template<size_t Index, size_t rows>
			static void recursiveTrainInnerWeights(ANNType & FFN, const std::array<DataType, rows> & Error, DataType LearningRate) {
				//constexpr size_t rows = std::get<Index + 1>(StaticThis::HiddenLayerSizes);
				constexpr size_t cols = std::get<Index>(StaticThis::HiddenLayerSizes);//

				const std::array<DataType, rows> & Net = std::get<Index>(FFN.NetLayers);
				const std::array<DataType, rows> & Output = std::get<Index>(FFN.HiddenLayers);
				const std::array<DataType, cols> & Input = std::get<Index - 1>(FFN.HiddenLayers);
				std::array<std::array<DataType, cols + 1>, rows> & WeightMatrix = std::get<Index>(FFN.WeightMatricies);

				std::array<DataType, cols>  GradientError;
				backpropagate<ActFunc, rows, cols, DataType>(Input, Net, Error, GradientError, WeightMatrix, LearningRate);
				recursiveTrainInnerWeights<Index - 1, cols>(FFN, GradientError, LearningRate);
			}

			template<>
			static void recursiveTrainInnerWeights<0, StaticThis::FirstHiddenLayerSize>(ANNType & FFN, const std::array<DataType, StaticThis::FirstHiddenLayerSize> & Error, DataType LearningRate) {
				constexpr size_t rows = std::get<1>(StaticThis::HiddenLayerSizes);
				constexpr size_t cols = std::get<0>(StaticThis::HiddenLayerSizes);

				const std::array<DataType, rows> & Net = std::get<0>(FFN.NetLayers);
				const std::array<DataType, rows> & Output = std::get<0>(FFN.HiddenLayers);
				const std::array<DataType, cols> & Input = FFN.InputLayer;
				std::array<std::array<DataType, cols + 1>, rows> & WeightMatrix = std::get<0>(FFN.WeightMatricies);

				std::array<DataType, cols>  GradientError;
				backpropagate<ActFunc, rows, cols, DataType>(Input, Net, Error, GradientError, WeightMatrix, LearningRate);
			}

		public:
			internalBackPropagationTrainer() {}
			~internalBackPropagationTrainer() {}


			static void train(ANNType & FFN, const std::array<DataType, ANNType::InputSize> & Input, const std::array<DataType, ANNType::OutputSize> & ExpectedOutput, DataType Learningrate) {
				std::array<DataType, ANNType::OutputSize> Actual = { 0 };
				FFN.compute(Input, Actual);

				constexpr size_t Index = ANNType::WeightMatrixCount;
				constexpr size_t rows = std::get<Index>(StaticThis::HiddenLayerSizes);

				std::array<DataType, rows> Error;
				computeErrorDerivative<ErrorType>(Actual, ExpectedOutput, Error);

				recursiveTrainInnerWeights<Index - 1, rows>(FFN, Error, Learningrate);
			}
		};


		void * Internal_BackPropTrainers[ErrorFunctionCOUNT] = { nullptr };
		void * Internal_Genetic[ErrorFunctionCOUNT] = { nullptr };

	public:
		NetworkTrainer();
		NetworkTrainer(int def_val);
		~NetworkTrainer();

		//initialize
		template<ErrorFunction ErrorType, typename DataType, ActivationFunction ActFunc, size_t ... LayerSizes>
		void randomize_weights(FeedForwardNetwork<DataType, ActFunc, LayerSizes...> & FFN, DataType Bounds) {
			using NetworkType = FeedForwardNetwork<DataType, ActFunc, LayerSizes...>;
			using InternalTrainerType = internalGeneticTrainer<NetworkType, ErrorType, DataType, ActFunc, LayerSizes...>;

			if (this->Internal_Genetic[ErrorType] == nullptr) {
				this->Internal_Genetic[ErrorType] = reinterpret_cast<void*>(new InternalTrainerType());
			}

			reinterpret_cast<InternalTrainerType*>(this->Internal_Genetic[ErrorType])->randomizeWeights(FFN, Bounds);
		}


		//default
		template<ErrorFunction ErrorType, typename DataType, ActivationFunction ActFunc, size_t ... LayerSizes>
		void train_backpropagation(FeedForwardNetwork<DataType, ActFunc, LayerSizes...> & FFN,
			const std::array<DataType, FeedForwardNetwork<DataType, ActFunc, LayerSizes...>::InputSize> & Input,
			const std::array<DataType, FeedForwardNetwork<DataType, ActFunc, LayerSizes...>::OutputSize> & ExpectedOutput, DataType TrainingRate) {

			using NetworkType = FeedForwardNetwork<DataType, ActFunc, LayerSizes...>;
			using InternalTrainerType = internalBackPropagationTrainer<NetworkType, ErrorType, DataType, ActFunc, LayerSizes...>;

			InternalTrainerType::train(FFN, Input, ExpectedOutput, TrainingRate);
		}

	};
}