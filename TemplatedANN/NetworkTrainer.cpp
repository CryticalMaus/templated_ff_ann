#include "stdafx.h"
#include "NetworkTrainer.h"

NeuralNetwork::NetworkTrainer::NetworkTrainer()
{

}

NeuralNetwork::NetworkTrainer::NetworkTrainer(int def_val)
{
}

NeuralNetwork::NetworkTrainer::~NetworkTrainer()
{
	for (int i = 0; i < ErrorFunctionCOUNT; ++i) {
		if (this->Internal_BackPropTrainers[i] != nullptr) {
			delete(this->Internal_BackPropTrainers[i]);
		}
	}
	for (int i = 0; i < ErrorFunctionCOUNT; ++i) {
		if (this->Internal_Genetic[i] != nullptr) {
			delete(this->Internal_Genetic[i]);
		}
	}
}