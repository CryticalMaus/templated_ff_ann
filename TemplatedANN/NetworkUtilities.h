#pragma once
#include <vector>
#include <array>

namespace NeuralNetwork {
	//ACTIVATION FUNCTIONS
	enum ActivationFunction {
		SIGMOID,
		TANH,
		SOFTMAX,
		LINEAR
	};



	//activation function
	template< ActivationFunction ActFuncType, typename type, size_t size>
	inline void applyActivationFunction(const std::array<type, size> & NetLayer, std::array<type, size> & ActivationLayer) {
		switch (ActFuncType) {
		case(ActivationFunction::SIGMOID):
		{
			for (size_t i = 0; i < size; ++i) {
				ActivationLayer[i] = 1.0 / (1 + exp(-NetLayer[i]));
			}
			break;
		}
		case(ActivationFunction::TANH):
		{
			for (size_t i = 0; i < size; ++i) {
				ActivationLayer[i] = std::tanh(NetLayer[i]);
			}
			break;
		}
		case(ActivationFunction::SOFTMAX):
		{
			type Denominator = 0;
			for (size_t i = 0; i < size; ++i) {
				ActivationLayer[i] = std::exp(NetLayer[i]);
				Denominator += ActivationLayer[i];
			}
			for (size_t i = 0; i < size; ++i) {
				ActivationLayer[i] /= Denominator;
			}
			break;
		}
		case(ActivationFunction::LINEAR):
		{
			for (size_t i = 0; i < size; ++i) {
				ActivationLayer[i] = NetLayer[i];
			}
			break;
		}
		default:
			throw std::exception("Activation Function not supported");
			break;
		}
	}

	template< ActivationFunction ActFuncType, size_t SliceLength, size_t size, typename type>
	inline void applyActivationFunction(std::_Array_const_iterator<type, size> NetLayer, std::_Array_iterator<type, size> ActivationLayer) {
		switch (ActFuncType) {
		case(ActivationFunction::SIGMOID):
		{
			for (size_t i = 0; i < SliceLength; ++i) {
				*(ActivationLayer++) = 1.0 / (1 + exp(-*(NetLayer++)));
			}
			break;
		}
		case(ActivationFunction::TANH):
		{
			for (size_t i = 0; i < SliceLength; ++i) {
				*(ActivationLayer++) = std::tanh(*(NetLayer++));
			}
			break;
		}
		case(ActivationFunction::SOFTMAX):
		{
			std::array<type, size>::iterator StartIterator = ActivationLayer;
			type Denominator = 0;
			for (size_t i = 0; i < SliceLength; ++i) {
				*(ActivationLayer) = std::exp(*(NetLayer++));
				Denominator += *(ActivationLayer++);
			}
			for (size_t i = 0; i < SliceLength; ++i) {
				*(StartIterator++) /= Denominator;
			}
			break;
		}
		case(ActivationFunction::LINEAR):
			for (size_t i = 0; i < SliceLength; ++i) {
				*(ActivationLayer++) = *(NetLayer++);
			}
			break;
		default:
			throw std::exception("Activation Function not supported");
			break;
		}
	}





	//activation function derivative
	template<ActivationFunction ActFuncType, typename type, size_t size>
	inline void applyErrorToActivationDerivative(const std::array<type, size> & NetLayer, const std::array<type, size> & ErrorSignal, std::array<type, size> & dErrordNetLayer) {
		switch (ActFuncType) {
		case(ActivationFunction::SIGMOID):
		{
			for (size_t i = 0; i < size; ++i) {
				type ret = 1.0 / (1 + exp(-NetLayer[i]));
				dErrordNetLayer[i] = ret * (1 - ret) * ErrorSignal[i];
			}
			break;
		}
		case(ActivationFunction::TANH):
		{
			for (size_t i = 0; i < size; ++i) {
				type ret = std::tanh(NetLayer[i]);
				dErrordNetLayer[i] = (1 - ret * ret) * ErrorSignal[i];
			}
			break;
		}
		case(ActivationFunction::SOFTMAX):
		{
			std::array<type, size> SoftMax = { 0 };
			applyActivationFunction<ActivationFunction::SOFTMAX>(NetLayer, SoftMax);
			dErrordNetLayer = { 0 };
			for (size_t i = 0; i < size; ++i) {//i = network layer
				for (size_t j = 0; j < size; ++j) {//j = activation layer
					if (i == j) {
						dErrordNetLayer[j] += ErrorSignal[i] * SoftMax[i] * (1 - SoftMax[i]);//dEdOi * dOidNi = dEdNi
					}
					else {
						dErrordNetLayer[j] -= ErrorSignal[i] * SoftMax[i] * SoftMax[j];
					}
				}
			}
			break;
		}
		case(ActivationFunction::LINEAR):
			dErrordNetLayer = ErrorSignal;
			break;
		default:
			throw std::exception("BAD ACTIVATION FUNCTION CALL! YOU CALLED THE DERIVATIVE FUNCTION USING SOFTMAX ARGUMENTS WITHOUT THE SOFTMAX ID");
		}
	}

	template<ActivationFunction ActFuncType, typename type, size_t size>
	inline void computeActivationFunctionDerivative(const std::array<type, size> & NetLayer, std::array<type, size> & ActivationDerivative) {
		switch (ActFuncType) {
		case(ActivationFunction::SIGMOID):
		{
			for (size_t i = 0; i < size; ++i) {
				type ret = 1.0 / (1 + exp(-NetLayer[i]));
				ActivationDerivative[i] = ret * (1 - ret);
			}
			break;
		}
		case(ActivationFunction::TANH):
		{
			for (size_t i = 0; i < size; ++i) {
				type ret = std::tanh(NetLayer[i]);
				ActivationDerivative[i] = (1 - ret * ret);
			}
			break;
		}
		case(ActivationFunction::SOFTMAX):
		{
			std::array<type, size> SoftMax = { 0 };
			applyActivationFunction<ActivationFunction::SOFTMAX>(NetLayer, SoftMax);
			ActivationDerivative = { 0 };
			for (size_t i = 0; i < size; ++i) {//i = network layer
				for (size_t j = 0; j < size; ++j) {//j = activation layer
					if (i == j) {
						ActivationDerivative[j] += SoftMax[i] * (1 - SoftMax[i]);//dEdOi * dOidNi = dEdNi
					}
					else {
						ActivationDerivative[j] -= SoftMax[i] * SoftMax[j];
					}
				}
			}
			break;
		}
		case(ActivationFunction::LINEAR):
			ActivationDerivative = { 1 };
			break;
		default:
			throw std::exception("BAD ACTIVATION FUNCTION CALL! YOU CALLED THE DERIVATIVE FUNCTION USING SOFTMAX ARGUMENTS WITHOUT THE SOFTMAX ID");
		}
	}




	//ERROR COMPUTATION
	enum ErrorFunction {
		SquaredError,
		CrossEntropy,//used for softmax
		ErrorFunctionCOUNT
	};

	template<ErrorFunction ErrorType, typename type, size_t Size> void computeError(const std::array<type, Size> & actual, const std::array<type, Size> & ideal, std::array<type, Size> & Error) {
		switch (ErrorType) {
		case(ErrorFunction::SquaredError):
		{
			for (size_t i = 0; i < Size; ++i) {
				Error[i] = std::pow(ideal[i] - actual[i], 2) / 2;
			}
			break;
		}
		case(ErrorFunction::CrossEntropy):
		{
			for (size_t i = 0; i < Size; ++i) {
				Error[i] = ideal[i] * std::log(actual[i]);
			}
			break;
		}
		default:
			throw std::exception("that error type is not supported");
		}
	}
	template<ErrorFunction ErrorType, typename type, size_t Size> type computeErrorSummation(const std::array<type, Size> & actual, const std::array<type, Size> & ideal) {
		type ErrorSum = 0;
		switch (ErrorType) {
		case(ErrorFunction::SquaredError):
		{
			for (size_t i = 0; i < Size; ++i) {
				ErrorSum += std::pow(ideal[i] - actual[i], 2) / 2;
			}
			return ErrorSum;
		}
		case(ErrorFunction::CrossEntropy):
		{
			for (size_t i = 0; i < Size; ++i) {
				ErrorSum += ideal[i] * std::log(actual[i] + FLT_EPSILON);
			}
			return ErrorSum;
		}
		default:
			throw std::exception("that error type is not supported");
		}
	}




	//error derivative
	template<ErrorFunction ErrorType, typename type, size_t Size> void computeErrorDerivative(const std::array<type, Size> & actual, const std::array<type, Size> & ideal, std::array<type, Size> & Error) {
		switch (ErrorType) {
		case(ErrorFunction::SquaredError):
		{
			for (size_t i = 0; i < Size; ++i) {
				Error[i] = actual[i] - ideal[i];
			}
			break;
		}
		case(ErrorFunction::CrossEntropy):
		{
			for (size_t i = 0; i < Size; ++i) {
				Error[i] = actual[i] - ideal[i];
			}
			break;
		}
		default:
			throw std::exception("that error type is not supported");
		}
	}
}