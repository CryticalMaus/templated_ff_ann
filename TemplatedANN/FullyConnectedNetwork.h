#pragma once

#include <string>
#include <tuple>
#include <array>
#include <vector>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <cstdarg>

#include "NetworkUtilities.h"

namespace NeuralNetwork {

	template<typename DataType, NeuralNetwork::ActivationFunction ActFunc,
		size_t ... LayerSizes>
		class FeedForwardNetwork
	{
		static_assert(sizeof...(LayerSizes) >= 3, "Must be given at least InputSize, HiddenSize, OutputSize");
	protected:
		using StaticThis = FeedForwardNetwork<DataType, ActFunc, LayerSizes...>;
		//struct that generates the tuple type of the weight matricies
		template<size_t ... T>
		struct WeightMatrixTupleType_Impl;

		template<size_t W, size_t H>
		struct WeightMatrixTupleType_Impl<W, H> {
			typedef typename std::tuple<std::array<std::array<DataType, W + 1>, H>> type;//+1 width for bias
		};

		template<size_t W, size_t H, size_t ... T>
		struct WeightMatrixTupleType_Impl<W, H, T...> {
			typedef decltype(std::tuple_cat(std::declval<typename WeightMatrixTupleType_Impl<W, H>::type>(), std::declval<typename WeightMatrixTupleType_Impl<H, T...>::type>())) type;
		};

		//struct that generates the tuple type of the hidden layers
		template<size_t ... T>
		struct HiddenLayerTupleType_Impl;

		template<size_t W, size_t H>
		struct HiddenLayerTupleType_Impl<W, H> {
			typedef typename std::tuple<std::array<DataType, H>> type;
		};

		template<size_t W, size_t H, size_t ... T>
		struct HiddenLayerTupleType_Impl<W, H, T...> {
			typedef decltype(std::tuple_cat(
				std::declval<typename HiddenLayerTupleType_Impl<W, H>::type>(),
				std::declval<typename HiddenLayerTupleType_Impl<H, T...>::type>()
			)) type;
		};

		//static members
		static constexpr size_t WeightMatrixCount = (sizeof...(LayerSizes)) - 1;
		static constexpr size_t HiddenLayerCount = (sizeof...(LayerSizes)) - 2;
		static constexpr size_t InputSize = std::get<0>(std::make_tuple(LayerSizes...));
		static constexpr size_t OutputSize = std::get<(sizeof...(LayerSizes)) - 1>(std::make_tuple(LayerSizes...));


		//object members
		std::array<DataType, StaticThis::InputSize> InputLayer;
		typename WeightMatrixTupleType_Impl<LayerSizes...>::type WeightMatricies;
		typename HiddenLayerTupleType_Impl<LayerSizes...>::type HiddenLayers, NetLayers;

		//recursive computation
		template<size_t Index>
		void recursiveCompute() {
			recursiveCompute<Index - 1>();
			constexpr size_t cols = std::get<Index - 1>(std::make_tuple(LayerSizes...));
			constexpr size_t rows = std::get<Index>(std::make_tuple(LayerSizes...));

			const std::array<std::array<DataType, cols + 1>, rows> & L = std::get<Index - 1>(this->WeightMatricies);
			const std::array<DataType, cols> & R = std::get<Index - 2>(this->HiddenLayers);


			//multiply
			std::array<DataType, rows> & OutLayer = std::get<Index - 1>(this->HiddenLayers);
			std::array<DataType, rows> & NetLayer = std::get<Index - 1>(this->NetLayers);
			for (size_t i = 0; i < rows; ++i) {
				NetLayer[i] = L[i][cols];//bias weight
				for (size_t k = 0; k < cols; ++k)
				{
					NetLayer[i] += L[i][k] * R[k];
				}
			}
			applyActivationFunction<ActFunc>(NetLayer, OutLayer);
		}

		template<>
		void recursiveCompute<1>() {
			constexpr size_t cols = std::get<0>(std::make_tuple(LayerSizes...));
			constexpr size_t rows = std::get<1>(std::make_tuple(LayerSizes...));

			const std::array<std::array<DataType, cols + 1>, rows> & L = std::get<0>(this->WeightMatricies);
			const std::array<DataType, cols> & R = this->InputLayer;

			//multiply
			std::array<DataType, rows> & OutLayer = std::get<0>(this->HiddenLayers);
			std::array<DataType, rows> & NetLayer = std::get<0>(this->NetLayers);
			for (size_t i = 0; i < rows; ++i) {
				NetLayer[i] = L[i][cols];//bias weight
				for (size_t k = 0; k < cols; ++k)
				{
					NetLayer[i] += L[i][k] * R[k];
				}
			}
			applyActivationFunction<ActFunc>(NetLayer, OutLayer);
		}


		//FILE IO FUNCTIONS
		//WRITE
		template<size_t Index>//Weight matricies
		void recursiveFileWriteWeights(std::ofstream & File) {
			recursiveFileWriteWeights<Index - 1>(File);
			constexpr size_t rows = std::get<Index + 1>(std::make_tuple(LayerSizes...));
			constexpr size_t cols = std::get<Index>(std::make_tuple(LayerSizes...));
			const std::array<std::array<DataType, cols>, rows> & matrix = std::get<Index>(this->WeightMatricies);
			File << "Matrix: " << Index << '\n';
			for (int y = 0; y < rows; ++y) {
				File << matrix[y][0];
				for (int x = 1; x < cols; ++x) {
					File << '\t' << matrix[y][x];
				}
				File << '\n';
			}

		}
		template<>
		void recursiveFileWriteWeights<0>(std::ofstream & File) {
			constexpr size_t rows = std::get<1>(std::make_tuple(LayerSizes...));
			constexpr size_t cols = std::get<0>(std::make_tuple(LayerSizes...));
			const std::array<std::array<DataType, cols>, rows> & matrix = std::get<0>(this->WeightMatricies);
			File << "Matrix: " << 0 << '\n';
			for (int y = 0; y < rows; ++y) {
				File << matrix[y][0];
				for (int x = 1; x < cols; ++x) {
					File << '\t' << matrix[y][x];
				}
				File << '\n';
			}
		}

		//READ
		template<size_t Index>//weight matricies
		void recursiveFileReadWeights(std::ifstream & File) {
			recursiveFileReadWeights<Index - 1>(File);
			constexpr size_t rows = std::get<Index + 1>(std::make_tuple(LayerSizes...));
			constexpr size_t cols = std::get<Index>(std::make_tuple(LayerSizes...));
			std::array<std::array<DataType, cols>, rows> & matrix = std::get<Index>(this->WeightMatricies);

			std::string line;
			std::getline(File, line);//consume "Matrix:  << Index << '\n'";
			for (int y = 0; y < rows; ++y) {
				for (int x = 0; x < cols; ++x) {
					File >> matrix[y][x];
				}
			}
			std::getline(File, line);//consume the end of line
		}
		template<>
		void recursiveFileReadWeights<0>(std::ifstream & File) {
			constexpr size_t rows = std::get<1>(std::make_tuple(LayerSizes...));
			constexpr size_t cols = std::get<0>(std::make_tuple(LayerSizes...));
			std::array<std::array<DataType, cols>, rows> & matrix = std::get<0>(this->WeightMatricies);

			std::string line;
			std::getline(File, line);//consume "Matrix:  << Index << '\n'";
			for (int y = 0; y < rows; ++y) {
				for (int x = 0; x < cols; ++x) {
					File >> matrix[y][x];
				}
			}
			std::getline(File, line);//consume the end of line
		}

		friend class NetworkTrainer;// <DataType, ActFunc, LayerSizes...>;


	public:


		FeedForwardNetwork() {
			assert(StaticThis::WeightMatrixCount >= 2);
		}
		~FeedForwardNetwork() {}

		//COMPUTATION
		void compute(const std::array<DataType, StaticThis::InputSize> & Input, std::array<DataType, StaticThis::OutputSize> & Output) {

			for (size_t i = 0; i < StaticThis::InputSize; ++i) {
				this->InputLayer[i] = Input[i];
			}

			constexpr size_t Index = StaticThis::WeightMatrixCount;
			constexpr size_t cols = std::get<Index - 1>(std::make_tuple(LayerSizes...));
			constexpr size_t rows = std::get<Index>(std::make_tuple(LayerSizes...));

			recursiveCompute<Index - 1>();

			const std::array<std::array<DataType, cols + 1>, rows> & L = std::get<Index - 1>(this->WeightMatricies);
			const std::array<DataType, cols> & R = std::get<Index - 2>(this->HiddenLayers);

			//multply
			std::array<DataType, rows> & NetLayer = std::get<Index - 1>(this->NetLayers);
			for (size_t i = 0; i < rows; ++i) {
				NetLayer[i] = L[i][cols];
				for (size_t k = 0; k < cols; ++k)
				{
					NetLayer[i] += L[i][k] * R[k];
				}
			}
			NeuralNetwork::applyActivationFunction<ActFunc>(NetLayer, Output);
		}


		//FILE IO
		bool write(std::ofstream & File) {
			assert(File.is_open());
			//write header
			File << "Neural Network\n";

			//write hidden layer count
			File << StaticThis::HiddenLayerCount << '\n';

			//write input size
			File << StaticThis::InputSize << '\n';

			//write output size
			File << StaticThis::OutputSize << '\n';

			//write matricies
			recursiveFileWriteWeights<StaticThis::WeightMatrixCount - 1>(File);

			return true;
		}

		bool read(std::ifstream & File) {
			assert(File.is_open());
			std::string line;
			//check header
			std::getline(File, line);
			assert(line == "Neural Network");

			//check hidden layer size
			std::getline(File, line);
			assert(line == std::to_string(StaticThis::HiddenLayerCount));

			//check input size
			std::getline(File, line);
			assert(line == std::to_string(StaticThis::InputSize));

			//check output size
			std::getline(File, line);
			assert(line == std::to_string(StaticThis::OutputSize));

			//check hidden layers
			//CheckHiddenLayerSizes(File, LayerSizes) ...;

			//recursively fill in matricies
			recursiveFileReadWeights<StaticThis::HiddenLayerCount>(File);

			return true;
		}
	};

}