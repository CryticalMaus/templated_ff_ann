This repository is a snippet from my personal machine learning library, which I have been developing in order to learn about machine learning.

This is a visual studio project.

FullyConnectedNetwork contains the FFN implementation. It is templated to handle any number of varying size hidden layers.

NetworkTrainer contains the backpropagation algorithm.

NetworkUtilities contains various activation functions and their derivatives.

The dataset the network trains on is simply a function that computes the correct output from the input. The network's goal is to learn that function.